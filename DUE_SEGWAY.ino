#include <Wire.h>
#include <I2Cdev.h>
#include <MPU6050.h>
#include <Kalman_Angle.h>
#include <PID.h>

#include "pin_allocation.h"
#include "IMU.h"
#include "encoder.h"
#include "state_variables.h"
#include "physical_parameters.h"

//CONTROL SYSTEM DEFINES
#define UPDATE_FREQ 100 //Hz

MPU6050 IMU = MPU6050 ();

Kalman_Angle angleFilter (0.001, 0.003, 0.03);
PID anglePID (1, 150, 0.01, 0.275, 100000, 255, -255);

const float target = 0.5;

volatile int pwm;

void setup ()
{
  Serial.begin (115200);
  Wire.begin ();
  
  IMU_setup ();
  encoder_setup ();
  motor_driver_setup ();
  TC3_setup ();
//  TC4_setup ();
}

void loop ()
{ 
//space for code that doesnt involve the control system

//  update_state_variables ();
  
//  Serial.print ("SYSTEM VARIABLES AT TIME: ");
//  Serial.println (millis());
//  dump_encoder ();
//  dump_translation ();

  Serial.print (millis ());
  Serial.print ("\t");
  Serial.print (STATE_x);
  Serial.print ("\t");
  Serial.println (STATE_theta);
  delay (100);
}

void control_update ()
{
  static long time = 0;
  long t = micros ();
  float dt = float ((t - time)) / 1000000;
  
  int16_t ax, ay, az;
  int16_t gx, gy, gz;
  //int16_t mx, my, mz;
  IMU.getMotion6 (&ax, &ay, &az, &gx, &gy, &gz);
  
  STATE_theta = angleFilter.update (AccelerationAngle (ax, az), GyroscopeRate (gy), dt);
  STATE_x = WHEEL_L_rotations*wheel_circumference/1000;
  STATE_y = 0;
  STATE_heading = 0;

  if (abs (STATE_theta) < 30)
  {
//    if (abs (STATE_x) > 0.2)
//      pwm = (int) anglePID.update (STATE_theta, STATE_x > 0 ? 0.05 : -0.1, dt);
//    else
      pwm = (int) anglePID.update (STATE_theta, 0, dt);
    set_left_motor (pwm);
    set_right_motor (pwm);
  }
  else
  {
    anglePID.reset ();
    WHEEL_L_rotations = 0;
    set_left_motor (0);
    set_right_motor (0);
  }
  
  time = t;
}

void update_theta ()
{
  //
}

void update_displacement ()
{
  //
}

/*

void update_state_variables ()
{
  //get wheel encoder data and compute dx, dv, da and x, v, a
  float dlx0 = WHEEL_L_rotations*wheel_circumference/1000 - displacement;
  float drx0 = WHEEL_R_rotations*wheel_circumference/1000 - displacement;
  
  delta_displacement = dlx0;

  delta_velocity = -1*velocity + delta_displacement / delta_time;
  delta_acceleration = -1*acceleration + delta_velocity / delta_time;
  
  displacement += delta_displacement;
  velocity += delta_velocity;
  acceleration += delta_acceleration;
}

void dump_translation ()
{
  Serial.println ("TRANSLATION");
  Serial.print ("\tLinear Displacement: ");
  Serial.print (displacement);
  Serial.println (" m");
  Serial.print ("\tLinear Velocity: ");
  Serial.print (velocity);
  Serial.println (" m/s");
  Serial.print ("\tLinear Acceleration: ");
  Serial.print (acceleration);
  Serial.println (" m/s^2");
  Serial.println ("");
}

void dump_delta_translation ()
{
  Serial.println ("DELTA TRANSLATION");
  Serial.print ("\tDelta Linear Displacement: ");
  Serial.print (delta_displacement);
  Serial.println (" m");
  Serial.print ("\tDelta Linear Velocity: ");
  Serial.print (delta_velocity);
  Serial.println (" m/s");
  Serial.print ("\tDelta Linear Acceleration: ");
  Serial.print (delta_acceleration);
  Serial.println (" m/s^2");
  Serial.println ("");
}

void dump_encoder ()
{
  Serial.println ("ENCODERS:");
  Serial.println ("LEFT:");
  Serial.print ("\tRotations: ");
  Serial.println (WHEEL_L_rotations);

  Serial.println ("RIGHT:");
  Serial.print ("\tRotations: ");
  Serial.println (WHEEL_R_rotations);
  Serial.println ("");
}

*/
