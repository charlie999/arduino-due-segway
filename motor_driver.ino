//ARDUMOTOR SETUP
void motor_driver_setup ()
{
  pinMode (DRIVER_DIR_A, OUTPUT);
  pinMode (DRIVER_DIR_B, OUTPUT);
  analogWrite (DRIVER_PWM_A, 0);
  analogWrite (DRIVER_PWM_B, 0);
}

void set_left_motor (int val)
{
  set_motor (DRIVER_DIR_A, DRIVER_PWM_A, -val);
}

void set_right_motor (int val)
{
  set_motor (DRIVER_DIR_B, DRIVER_PWM_B, val);
}
  
void set_motor (int DIR_PIN, int PWM_PIN, int val)
{
  val > 0 ? digitalWrite (DIR_PIN, HIGH) : digitalWrite (DIR_PIN, LOW);
  analogWrite (PWM_PIN, abs (val));
}  

/*
float DRIVER_A_current ()
{
  return (3.3*(analogRead (DRIVER_A_sense)/(2 ^ 12))/DRIVER_A_resistance);
}

float DRIVER_B_current ()
{
  return (3.3*(analogRead (DRIVER_B_sense)/(2 ^ 12))/DRIVER_B_resistance);
}
*/
