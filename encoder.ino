//QUADRATURE ENCODER SETUP
void encoder_setup ()
{
  pinMode (WHEEL_L_POWER, OUTPUT);
  pinMode (WHEEL_R_POWER, OUTPUT);
  digitalWrite (WHEEL_L_POWER, HIGH);
  digitalWrite (WHEEL_R_POWER, HIGH);
  
  pinMode (WHEEL_L_A_pin, INPUT);
  pinMode (WHEEL_L_B_pin, INPUT);

  pinMode (WHEEL_R_A_pin, INPUT);
  pinMode (WHEEL_R_B_pin, INPUT);

  attachInterrupt (WHEEL_L_A_pin, WHEEL_L_A_int, CHANGE);
  attachInterrupt (WHEEL_L_B_pin, WHEEL_L_B_int, CHANGE);
  
  attachInterrupt (WHEEL_R_A_pin, WHEEL_R_A_int, CHANGE);
  attachInterrupt (WHEEL_R_B_pin, WHEEL_R_B_int, CHANGE);
}

//ENCODER INTERRUPTS

void WHEEL_L_A_int ()
{
  if ((digitalRead (WHEEL_L_A_pin) && !digitalRead (WHEEL_L_B_pin)) || (!digitalRead (WHEEL_L_A_pin) && digitalRead (WHEEL_L_B_pin)))
  {
    if (WHEEL_L_forward)
      WHEEL_L_rotations += WHEEL_L_STEP;
    WHEEL_L_forward = true;
  }
  else
  {
    if (!WHEEL_L_forward)
      WHEEL_L_rotations -= WHEEL_L_STEP;
    WHEEL_L_forward = false;
  }
}

void WHEEL_L_B_int ()
{
  if ((digitalRead (WHEEL_L_A_pin) && digitalRead (WHEEL_L_B_pin)) || (!digitalRead (WHEEL_L_A_pin) && !digitalRead (WHEEL_L_B_pin)))
  {
    if (WHEEL_L_forward)
      WHEEL_L_rotations += WHEEL_L_STEP;
    WHEEL_L_forward = true;
  }
  else
  {
    if (!WHEEL_L_forward)
      WHEEL_L_rotations -= WHEEL_L_STEP;
    WHEEL_L_forward = false;
  }
}

void WHEEL_R_A_int ()
{
  if ((digitalRead (WHEEL_R_A_pin) && !digitalRead (WHEEL_R_B_pin)) || (!digitalRead (WHEEL_R_A_pin) && digitalRead (WHEEL_R_B_pin)))
  {
    if (WHEEL_R_forward)
      WHEEL_R_rotations += WHEEL_R_STEP;
    WHEEL_R_forward = true;
  }
  else
  {
    if (!WHEEL_R_forward)
      WHEEL_R_rotations -= WHEEL_R_STEP;
    WHEEL_R_forward = false;
  }
}

void WHEEL_R_B_int ()
{
  if ((digitalRead (WHEEL_R_B_pin) && digitalRead (WHEEL_R_A_pin)) || (!digitalRead (WHEEL_R_B_pin) && !digitalRead (WHEEL_R_A_pin)))
  {
    if (WHEEL_R_forward)
      WHEEL_R_rotations += WHEEL_R_STEP;
    WHEEL_R_forward = true;
  }
  else
  {
    if (!WHEEL_R_forward)
      WHEEL_R_rotations -= WHEEL_R_STEP;
    WHEEL_R_forward = false;
  }
}

/*

void WHEEL_L_A_int ()
{
  if ((digitalRead (WHEEL_L_A_pin) && !digitalRead (WHEEL_L_B_pin)) || (!digitalRead (WHEEL_L_A_pin) && digitalRead (WHEEL_L_B_pin)))
  {
    //Only add to rotations if a full transition has occured ie the direction didnt change half way through a state
    if (WHEEL_L_forward)
    {
      //ALBL->AHBL
      if (digitalRead (WHEEL_L_A_pin) && !digitalRead (WHEEL_L_B_pin))
      {
        WHEEL_L_rotations += WHEEL_L_ALBL * WHEEL_L_dir;
      }
      //AHBH->ALBH
      else
      {
        WHEEL_L_rotations += WHEEL_L_AHBH * WHEEL_L_dir;
      }
    }
    WHEEL_L_forward = true;
  }
  else
  {
    //Only subtract from rotations if a full transition has occured
    if (!WHEEL_L_forward)
    {
      //AHBL->ALBL
      if (!digitalRead (WHEEL_L_A_pin) && !digitalRead (WHEEL_L_B_pin))
      {
        WHEEL_L_rotations -= WHEEL_L_AHBL * WHEEL_L_dir;
      }
      //ALBH->AHBH
      else
      {
        WHEEL_L_rotations -= WHEEL_L_ALBH * WHEEL_L_dir;
      }
    }
    WHEEL_L_forward = false;
  }
}

void WHEEL_L_B_int ()
{
  if ((digitalRead (WHEEL_L_A_pin) && digitalRead (WHEEL_L_B_pin)) || (!digitalRead (WHEEL_L_A_pin) && !digitalRead (WHEEL_L_B_pin)))
  {
    //Only add to rotations if a full transition has occured ie the direction didnt change half way through a state
    if (WHEEL_L_forward)
    {
      //AHBL->AHBH
      if (digitalRead (WHEEL_L_A_pin) && digitalRead (WHEEL_L_B_pin))
      {
        WHEEL_L_rotations += WHEEL_L_AHBL * WHEEL_L_dir;
      }
      //ALBH->ALBL
      else
      {
        WHEEL_L_rotations += WHEEL_L_ALBH * WHEEL_L_dir;
      }
    }
    WHEEL_L_forward = true;
  }
  else
  {
    //Only subtract from rotations if a full transition has occured
    if (!WHEEL_L_forward)
    {
      //AHBH->AHBL
      if (digitalRead (WHEEL_L_A_pin) && !digitalRead (WHEEL_L_B_pin))
      {
        WHEEL_L_rotations -= WHEEL_L_AHBH * WHEEL_L_dir;
      }
      //ALBL->ALBH
      else
      {
        WHEEL_L_rotations -= WHEEL_L_ALBL * WHEEL_L_dir;
      }
    }
    WHEEL_L_forward = false;
  }
}

void WHEEL_R_A_int ()
{
  if ((digitalRead (WHEEL_R_A_pin) && !digitalRead (WHEEL_R_B_pin)) || (!digitalRead (WHEEL_R_A_pin) && digitalRead (WHEEL_R_B_pin)))
  {
    //Only add to rotations if a full transition has occured ie the direction didnt change half way through a state
    if (WHEEL_R_forward)
    {
      //ALBL->AHBL
      if (digitalRead (WHEEL_R_A_pin) && !digitalRead (WHEEL_R_B_pin))
      {
        WHEEL_R_rotations += WHEEL_R_ALBL * WHEEL_R_dir;
      }
      //AHBH->ALBH
      else
      {
        WHEEL_R_rotations += WHEEL_R_AHBH * WHEEL_R_dir;
      }
    }
    WHEEL_R_forward = true;
  }
  else
  {
    //Only subtract from rotations if a full transition has occured
    if (!WHEEL_R_forward)
    {
      //AHBL->ALBL
      if (!digitalRead (WHEEL_R_A_pin) && !digitalRead (WHEEL_R_B_pin))
      {
        WHEEL_R_rotations -= WHEEL_R_AHBL * WHEEL_R_dir;
      }
      //ALBH->AHBH
      else
      {
        WHEEL_R_rotations -= WHEEL_R_ALBH * WHEEL_R_dir;
      }
    }
    WHEEL_R_forward = false;
  }
}

void WHEEL_R_B_int ()
{
  if ((digitalRead (WHEEL_R_B_pin) && digitalRead (WHEEL_R_A_pin)) || (!digitalRead (WHEEL_R_B_pin) && !digitalRead (WHEEL_R_A_pin)))
  {
    //Only add to rotations if a full transition has occured ie the direction didnt change half way through a state
    if (WHEEL_R_forward)
    {
      //AHBL->AHBH
      if (digitalRead (WHEEL_R_A_pin) && digitalRead (WHEEL_R_B_pin))
      {
        WHEEL_R_rotations += WHEEL_R_AHBL * WHEEL_R_dir;
      }
      //ALBH->ALBL
      else
      {
        WHEEL_R_rotations += WHEEL_R_ALBH * WHEEL_R_dir;
      }
    }
    WHEEL_R_forward = true;
  }
  else
  {
    //Only subtract from rotations if a full transition has occured
    if (!WHEEL_R_forward)
    {
      //AHBH->AHBL
      if (digitalRead (WHEEL_R_A_pin) && !digitalRead (WHEEL_R_B_pin))
      {
        WHEEL_R_rotations -= WHEEL_R_AHBH * WHEEL_R_dir;
      }
      //ALBL->ALBH
      else
      {
        WHEEL_R_rotations -= WHEEL_R_ALBL * WHEEL_R_dir;
      }
    }
    WHEEL_R_forward = false;
  }
}

*/
