//WHEEL ENCODER DEFINES
#define WHEEL_L_POWER 53
#define WHEEL_R_POWER 52
#define WHEEL_L_A_pin 51
#define WHEEL_L_B_pin 49
#define WHEEL_R_A_pin 50
#define WHEEL_R_B_pin 48

//MPU 9150 DEFINES
/* NONE */

//MOTOR DRIVER DEFINES
#define DRIVER_PWM_A 3
#define DRIVER_PWM_B 11
#define DRIVER_DIR_A 12
#define DRIVER_DIR_B 13

//
