//TC1.0 interrupt handler
void TC3_Handler ()
{
  TC_GetStatus (TC1, 0);
  control_update ();
//  update_state_variables ();
}

//TC1.1 interrupt handler
void TC4_Handler ()
{
  TC_GetStatus (TC1, 1);
}

void TC3_setup ()
{
  pmc_set_writeprotect (false);
  pmc_enable_periph_clk (TC3_IRQn);
  
  TC1->TC_CHANNEL[0].TC_CMR = TC_CMR_TCCLKS_TIMER_CLOCK1 | TC_CMR_CPCTRG;
//  TC1->TC_CHANNEL[0].TC_RC = update_rate;
  TC1->TC_CHANNEL[0].TC_RC = 1000000 * clockCyclesPerMicrosecond () / (2 * UPDATE_FREQ);
  
  TC1->TC_CHANNEL[0].TC_IER = TC_IER_CPCS;
  
  NVIC_EnableIRQ(TC3_IRQn);
  
  TC_Start(TC1, 0);
}

void TC4_setup ()
{
  pmc_set_writeprotect (false);
  pmc_enable_periph_clk (TC4_IRQn);
  
  TC1->TC_CHANNEL[1].TC_CMR = TC_CMR_TCCLKS_TIMER_CLOCK1 | TC_CMR_CPCTRG;
  TC1->TC_CHANNEL[1].TC_RC = 1000000 * clockCyclesPerMicrosecond () / (2 * UPDATE_FREQ) * 2;
  
  TC1->TC_CHANNEL[1].TC_IER = TC_IER_CPCS;
  
  NVIC_EnableIRQ (TC4_IRQn);
  
  TC_Start (TC1, 1);
}
