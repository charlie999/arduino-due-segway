//LEFT WHEEL PARAMETERS
//VARIABLES
#define WHEEL_L_count 48;
volatile float WHEEL_L_rotations = 0;
volatile bool WHEEL_L_forward = true;
//volatile int WHEEL_L_dir = 1;

//LEFT WHEEL ROTATION COMPONENT
const float WHEEL_L_STEP = 1.0 / WHEEL_L_count;
/*
float WHEEL_L_ALBL = 1.0/WHEEL_L_count;
float WHEEL_L_AHBL = 1.0/WHEEL_L_count;
float WHEEL_L_AHBH = 1.0/WHEEL_L_count;
float WHEEL_L_ALBH = 1.0/WHEEL_L_count;
*/


//RIGHT WHEEL PARAMETERS
//VARIABLES
#define WHEEL_R_count 48;
volatile float WHEEL_R_rotations = 0;
volatile bool WHEEL_R_forward = true;
//volatile int WHEEL_R_dir = -1;

//RIGHT WHEEL ROTATION COMPONENT
const float WHEEL_R_STEP = 1.0 / WHEEL_R_count;
/*
float WHEEL_R_ALBL = 1.0/WHEEL_R_count;
float WHEEL_R_AHBL = 1.0/WHEEL_R_count;
float WHEEL_R_AHBH = 1.0/WHEEL_R_count;
float WHEEL_R_ALBH = 1.0/WHEEL_R_count;
*/
