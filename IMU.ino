
void IMU_setup ()
{
  pinMode (IMU_VCC, OUTPUT);
  pinMode (IMU_GND, OUTPUT);
  digitalWrite (IMU_VCC, HIGH);
  digitalWrite (IMU_GND, LOW);
  delay (100);
  IMU.initialize ();
}

float AccelerationAngle (int16_t a1, int16_t a2)
{
  static float scale = (float) IMU_ACCEL_SCALE / pow (2, IMU.getFullScaleAccelRange ());
  
  return 360 / (2 * PI) * atan2 ((float) a1 / scale, (float) a2 / scale) - 90;
}

float GyroscopeRate (int16_t g)
{
  static float scale = (float) IMU_GYRO_SCALE / pow (2, IMU.getFullScaleGyroRange ());
  
  return - (float) g / scale;
}
